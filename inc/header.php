<?php session_start(); ?>

<!DOCTYPE html>

<html lang="zxx">
<head>
	<title>Ma page de mon site</title>
	<meta charset="UTF-8">
	<link href="style/style.css" type="text/css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald%7CPassion+One%7CTitillium+Web" rel="stylesheet">

	<script src="js/jquery-3.1.1.min.js"></script>
</head>

<body>

<header>
	<nav>
		<div>
			<h1>C'est le titre de ma page</h1>
			<ul>
				<li><a href="/">Accueil</a></li>
				<li><a href="player.php">Albums</a></li>
			</ul>

		</div>
		<div>

			<?php if (!isset ($_SESSION['login'])) { ?>

				<form method="POST" action="connect.php">
					<input type="text" name="login" placeholder="Login">
					<input type="password" name="pwd" placeholder="Mot de passe">
					<input type="submit" value="Connexion">
				</form>

			<?php } else { ?>

				<form method="POST" action="disconnect.php">
					<input type="submit" value="Déconnexion">
				</form>

			<?php } ?>
		</div>
	</nav>

</header>