$(".file_path").hide();

$(".play-btn").click(function () {
    console.log("Play click");
    var path = $(this).nextAll(".file_path").html();
    var title = $(this).nextAll(".title").html();
    console.log(title);

    $("audio").attr("src", "music/" + path);
    $("#player-title").text(title);

    $("audio").play();
});