  <?php
	include "inc/header.php";
  ?>
  
	<div class="inner">
	
		<div class="bloc">
			<h2>Un tableau d'images</h2>
			
			<form action="index.php" method="POST">
				<input name="cols" type="range" value="2" min="1" max="4" step="1">
				<select name="theme">
					<?php
						if ($dir = opendir('img')){
							while (($fichier = readdir($dir)) !== false){
								if ($fichier[0] != "."){
									echo "<option value='$fichier'>$fichier</option>";
								}
							}
						}
					?>
				</select>
				<input type="submit" value="Voir">
			</form>
			
			<?php
				if (isset ($_POST['theme'], $_POST['cols'])){
					$subdir = $_POST['theme'];
					$cols = $_POST['cols'];
				
					echo "
						<table>
							<tbody>
					";
					
					if ($dir = opendir("img/" . $subdir)){
						$k = - 2;
						while (($fichier = readdir($dir)) !== false){
							if ($k == 0){echo "<tr>";}
							
							if ($fichier[0] != "."){
								echo "
									<td>
										<img src='img/$subdir/$fichier' alt='$fichier'>
									</td>
								";
							}
							
							if ($k == $cols-1){echo "</tr>";}
							$k++;
							if ($k == $cols){$k = 0;}
						}
						if ($k != 0){
							while ($k != $cols){
								$k++;
								echo "<td></td>";
							}
							echo "</tr>";
						}
					}
					
					echo "
							</tbody>
						</table>
					";
				}
			?>
			
		</div>
	
		<div class="bloc">
			<h2>Un dossier</h2>
			<table>
				<thead>
					<tr>
						<td>Nom du fichier</td>
						<td>Taille du fichier</td>
					</tr>
				</thead>
				<tbody>
			<?php
			
				if ($dir = opendir("img")){
					while (($fichier = readdir($dir)) !== false){
						if ($fichier != "." && $fichier != ".."){
							echo "<tr>";
							echo "<td>" . $fichier . "</td>";
							echo "<td>" . filesize("img/$fichier") . " octets </td>";
							echo "</tr>";
						}
					}
				}
			?>
				</tbody>
			</table>
		</div>
	
		<div class="bloc">
		
			<h2>Un formulaire</h2>
			
			<form action="index.php" method="post">
				<input type="text" name="prenom" placeholder="Prénom">
				<input type="text" name="nom" placeholder="Nom">
				<select name="size">
					<?php
					for ($i=0; $i < 101; $i++){
						echo "<option value='$i'>$i</option>";
					}
					?>
				</select>
				<input type="submit" value="Envoyer">
			</form>
			
			
			<?php
			if (isset($_POST['nom'], $_POST['prenom'])){
				echo "<h1>Félicitation " . $_POST['prenom'] . " " . $_POST['nom'] . "</h1>";
				echo "<h3>Vous avez gagné " . $_POST['size'] . "." . rand(999, 100) . ".000 euros !!!!</h3>";
			}
			?>
		</div>
		
		<div class="bloc">
			<h2>Un petit paragraphe de texte</h2>

			<?php echo file_get_contents('http://loripsum.net/api/1/short'); ?>
		</div>
		
		<div class="bloc">
			<h2>Un second paragraphe image</h2>
			
			<img src="lorem.jpg" alt="lorem" width="200">
			
		</div>
			
		<div class="bloc">
			
			<h2>Enfin, une série de blocs de couleur</h2>
			
			<p>Cliquez donc !!!</p>
			
			<div class="container">
			  <div class="element bleu">Un bloc bleu</div>
			  <div class="element vert">Un bloc vert</div>
			  <div class="element orange">Un bloc orange</div>
			  <div class="element cyan">Un bloc cyan</div>
			  <div class="element rouge">Un bloc rouge</div>
			  
			</div>
		</div>
		
		<div class="bloc">
			<h2>Un lecteur audio</h2>
			<div class="player-box">
				<img src="music/Atom_OS/artwork.jpg" alt="Atom OS">
				<audio src="music/Atom_OS/02-Orbital_Theory.mp3" controls></audio>
				<h2>02 - Orbital Theory - AtomOS</h2>
				
			</div>
		</div>
		
		<div class="bloc">
		
			<h2>Un tableau</h2>
			
			<table>
			  <thead>
				  <tr>
					<td>Un élément</td>
					<td>Un autre</td>
					<td>Un dernier</td>
				  </tr>
			  </thead>
			  <tbody>
				  <tr>
					<td>Un case</td>
					<td>Dat case</td>
					<td>Wow, much case</td>
				  </tr>
				  <tr>
					<td>Swagy case</td>
					<td>That case</td>
					<td>Owi la case</td>
				  </tr>
				  <tr>
					<td>Swagy case</td>
					<td>That case</td>
					<td>Bim la case</td>
				  </tr>
				  <tr>
					<td>Pimpe ma case</td>
					<td>That case</td>
					<td>Owi la case</td>
				  </tr>
			  <tbody>
			</table>
		
		</div>
	</div>	
    
  </body>
</html>