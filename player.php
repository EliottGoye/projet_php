<?php
include "inc/header.php";

$db_host = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "projet_asur";

$db = new PDO("mysql:dbname=$db_name;host=$db_host", $db_user, $db_pass);

?>

<div class="music">
    <div class="player">
        <div class="player-box">
            <img src="music/artwork.jpg" alt="Album">
            <audio src="music/*******" controls></audio>
            <h2 id="player-title">Pas d'écoute en cours</h2>
        </div>
    </div>

    <div class="playlist">
        <table>
            <thead>
            <tr>
                <th>Lecture</th>
                <th>ID</th>
                <th>Titre</th>
                <th>Artiste</th>
                <th>Album</th>
            <tr>
            </thead>
            <tbody>

            <?php

            $sql = "SELECT * FROM songs";

            $songs = $db->query($sql);

            foreach($songs as $song) {

                ?>

                <tr>
                    <td class="play-btn"><div class="play"></div></td>
                    <td class="song_id"><?php print $song['song_id']; ?></td>
                    <td class="title"><?php print $song['title']; ?></td>
                    <td class="artist"><?php print $song['artist']; ?></td>
                    <td class="album"><?php print $song['album']; ?></td>
                    <td class="file_path"><?php print $song['file_path'] ?></td>
                </tr>

            <?php }?>

            </tbody>

        </table>

    </div>

</div>

<script src="js/player.js"></script>