DROP DATABASE IF EXISTS projet_asur;

CREATE DATABASE projet_asur;

use projet_asur;

CREATE TABLE users (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login varchar(40),
    pass varchar(60)
    );
	
INSERT INTO users (login, pass) VALUES ('eliott', MD5('password'));

CREATE TABLE songs (
  id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  song_id int,
  title varchar (80),
  artist varchar(40),
  album varchar(60),
  file_path varchar(200)
);

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('1', 'Overture', 'AlexBC', 'AtomOs', 'Atom_OS/01 - Overture.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('2', 'Orbital_Theory', 'AlexBC', 'AtomOs', 'Atom_OS/02 - Orbital_Theory.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('3', 'Skyline Momentum', 'AlexBC', 'AtomOs', 'Atom_OS/03 - Skyline Momentum.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('4', 'Polonium', 'AlexBC', 'AtomOs', 'Atom_OS/04 - Polonium.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('5', 'Plasma', 'AlexBC', 'AtomOs', 'Atom_OS/05 - Plasma.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('6', 'Waves', 'AlexBC', 'AtomOs', 'Atom_OS/06 - Waves.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('7', 'Stereo Elimination', 'AlexBC', 'AtomOs', 'Atom_OS/07 - Stereo Elimination.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('8', 'Melting Core', 'AlexBC', 'AtomOs', 'Atom_OS/08 - Melting Core.mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('9', 'Made of Quarks Part I (Bottom and Strange)', 'AlexBC', 'AtomOs', 'Atom_OS/09 - Made of Quarks Part I (Bottom and Strange).mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('10', 'Made of Quarks Part II (Charm and Beauty)', 'AlexBC', 'AtomOs', 'Atom_OS/10 - Made of Quarks Part II (Charm and Beauty).mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('11', 'Made of Quarks Part III (Up and Truth)', 'AlexBC', 'AtomOs', 'Atom_OS/11 - Made of Quarks Part III (Up and Truth).mp3');

INSERT INTO songs (song_id, title, artist, album, file_path)
VALUES ('12', '[Bonus] One Robot Love (Mettatons Musical)', 'AlexBC', 'AtomOs', 'Atom_OS/12 - [Bonus] One Robot Love (Mettatons Musical).mp3');
